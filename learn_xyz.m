[x,y] = meshgrid(1:1:4, 2:1:5);
    
x
y

v = x;
xbar = 0.25*(v(1:end-1, 1:end-1) + v(1:end-1, 2:end) + v(2:end, 1:end-1) + v(2:end,2:end));

v = y;
ybar = 0.25*(v(1:end-1, 1:end-1) + v(1:end-1, 2:end) + v(2:end, 1:end-1) + v(2:end,2:end));


x0 = 1.7;
y0 = 1.7;

dist = sqrt((xbar-x0).^2 + (ybar-y0).^2);
[minVal, idx] = min(dist(:));
[row,col] = ind2sub(size(dist), idx);
row
col


x0 = 2.7;
y0 = 2.7;

dist = sqrt((xbar-x0).^2 + (ybar-y0).^2);
[minVal, idx] = min(dist(:));
[row,col] = ind2sub(size(dist), idx);
row
col



