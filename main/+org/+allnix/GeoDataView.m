classdef GeoDataView < ...
        matlab.mixin.SetGetExactNames & org.allnix.ListenerHandleList
    %GEODATAVIEW Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        id;
        uuid;
        %listenerHandleList;
    end
    
    methods (Static)
        
    end
    
    methods
        function me = GeoDataView()
        me.uuid = char(java.util.UUID.randomUUID.toString());
        %me.listenerHandleList = {};
        
        end
              
        function theDataChangedCallback(me, src, caseEventData)
        % This is how to get data
        TheData = caseEventData.data;
        %disp(caseEventData);
        disp(['theDataChanged: ', me.uuid]);
        disp(me.id);
        %disp(src);
       
        end
        
        function logDataChangedCallback(me, src, caseEventData)
        LogData = caseEventData.data;
        end
        
%         function addListenerHandle(me, lh)
%         me.listenerHandleList(end+1) = {lh};
%         end
        
%         function delete(me)
%         %Destructor
%         %cellfun(@(lh) delete(lh), me.listenerHandleList);
%         for ind = 1:length(me.listenerHandleList)
%             delete(me.listenerHandleList{ind});
%         end
%         end

        function delete(me)
        end
    end
end

