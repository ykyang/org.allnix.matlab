classdef SingleCaseManager < matlab.mixin.SetGetExactNames
    %SINGLECASEMANAGER Summary of this class goes here
    %   Detailed explanation goes here
    
    events
        TheDataChangedEvent
        LogDataChangedEvent
        GeoDataChangedEvent
    end
    
    properties
        TheData;
    end
    
    methods
        function lh = addTheDataChangedListener(me, listener)
        lh = addlistener(me, 'TheDataChangedEvent', ...
            @(src,caseEventData) listener.theDataChangedCallback(src, caseEventData)); 
        listener.addListenerHandle(lh);
%         if isempty(listener.listenerList)
%          listener.listenerList = {lh};
%         else 
%             listener.listenerList(end+1) = {lh};
%         end
        end
        
        function notifyTheDataChanged(me)
        caseEventData = org.allnix.CaseEventData(me.TheData);
        me.notify('TheDataChangedEvent', caseEventData);
        end
        
        function lh = addLogDataChangedListener(me, listener)
        lh = addlistener(me, 'LogDataChangedEvent', ...
            @(src, caseEventData) listener.logDataChangedCallback(src, caseEventData));
        listener.addListenerHandle(lh);
        end
        
        function notifyLogDataChanged(me)
        caseEventData = org.allnix.CaseEventData(me.TheData.LogData);
        me.notify('LogDataChangedEvent', caseEventData);
        end
    end
end

