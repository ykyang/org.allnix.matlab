classdef ListenerHandleList < matlab.mixin.SetGetExactNames
    %LISTENERHANDLELIST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        listenerHandleList;        
    end
    
    methods
        function obj = ListenerHandleList()
        %disp('construct from list');
        %LISTENERHANDLELIST Construct an instance of this class
        %   Detailed explanation goes here
        obj.listenerHandleList = {};
        end
        
        function addListenerHandle(me, lh)
        me.listenerHandleList(end+1) = {lh};
        end
        
        function delete(me)
        
        %Destructor
        for ind = 1:length(me.listenerHandleList)
            delete(me.listenerHandleList{ind});
        end
        %me.listenerHandleList = {};
        %disp('delete from list');
        end
    end
end

