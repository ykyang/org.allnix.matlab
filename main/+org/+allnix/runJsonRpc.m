function response = runJsonRpc(request)
%JSONRPCDRIVER Summary of this function goes here
%   Detailed explanation goes here

method = request.getMethod();

response = org.allnix.JsonRpcResponse();
response.setId(request.getId());

switch method
    case 'demo'
        %params = request.getParams();
        %file = params.file;
        % load Data from file
        % call compute(Data);
        
        %response = org.allnix.JsonRpcResponse();
        % convert response into text
        result = struct;
        result.value = 'demo';
        response.setResult(result);
    otherwise
%         response = org.allnix.JsonRpcResponse();
%         response.setId(request.getId());
        response.setErrorCode(-32601);
        response.setErrorMessage(sprintf('Unknown method: %s', method));
%         ME = MException(org.allnix.Exception.UnsupportedOperation, 'Unknown method: %s', method);
%         throw(ME);
end
end

