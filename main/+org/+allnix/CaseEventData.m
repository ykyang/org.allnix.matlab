classdef (ConstructOnLoad) CaseEventData < event.EventData
    %CASEEVENTDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        data;
    end
    
    methods
        function obj = CaseEventData(data)
        obj = obj@event.EventData();
        obj.data = data;
        end
    end
end

