% Create runner
%runner = matlab.unittest.TestRunner.withNoPlugins;
runner = matlab.unittest.TestRunner.withTextOutput;
plugin = matlab.unittest.plugins.TestRunProgressPlugin.withVerbosity(3);
runner.addPlugin(plugin);

% plugin = matlab.unittest.plugins.DiagnosticsOutputPlugin;
% runner.addPlugin(plugin);


org.allnix.setupLogger;

suite = matlab.unittest.TestSuite.fromPackage('org.allnix');
result = runner.run(suite);
disp(result);