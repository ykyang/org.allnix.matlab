logger = logging.getLogger('org.allnix');
if isfile('org.allnix.log')
    delete 'org.allnix.log';
end
logger.setFilename('org.allnix.log');
logger.setLogLevel(logging.logging.INFO);
logger.setCommandWindowLevel(logging.logging.WARNING);