classdef LayoutTest < matlab.unittest.TestCase
    %LAYOUTTEST Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Property1
    end
    
    methods (Test)
        function testVBox(me)
        % suite = matlab.unittest.TestSuite.fromClass(?org.allnix.LayoutTest, 'ProcedureName', 'testVBox');
        fig = figure;
        
        
        vbox = uix.VBox('Parent', fig);
        p = uipanel('Parent', vbox);
        Axes = axes('Parent', p, 'ActivePositionProperty', 'outerposition');
        hold(Axes, 'on');
        h = surf(Axes, peaks(25));
        axis(Axes, 'vis3d');
        
        hbox = uix.HBox('Parent', vbox);
        hbox.Padding = 1;
        vbox.Heights= [-1, 19];
        
        label = uicontrol('Parent', hbox, 'Style', 'text', 'String', 'N x N: ');
        label.HorizontalAlignment = 'right';
        v = uicontrol('Parent', hbox, 'Style', 'slider', 'Min', 2, 'Max', 50, 'SliderStep', [1/48,5/48]);
        hbox.Widths = [50, -1];
        v.Value = 25;
        v.Callback = @callback;
        %vbox.Heights= [-1, 17];
        %hold(Axes, 'off');
            function callback(src, eventdata)
            %hold(Axes, 'on');
            src.Value = floor(src.Value);
            delete(h);
            h = surf(Axes, peaks(src.Value));
%             disp(src);
%             disp(eventdata);
            end
        
        end
    end
    
end

