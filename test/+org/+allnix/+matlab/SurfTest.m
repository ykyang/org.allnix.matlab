classdef SurfTest < matlab.unittest.TestCase
    % Learn 'surf' function
    %   
    %
    %   suite = matlab.unittest.TestSuite.fromClass(?org.allnix.matlab..SurfTest);
    %   result = run(suite)
    %   disp(result)
    properties
       
    end
    
    methods (Static)
        function Figure = newFigure()
        Figure = figure;
        Figure.RendererMode = 'manual';
        Figure.Renderer = 'painters';
        end
        
        function Axes = newAxes()
        Axes = axes;
        
        axis(Axes, 'equal');
        axis(Axes, 'vis3d');
        
        Axes.Projection = 'perspective';
        Axes.ZDir = 'reverse';
        Axes.DataAspectRatio = [1,1,1];
        Axes.Clipping = 'off';
        Axes.View = [90, 0];
        
        grid(Axes, 'on');
        end
    end
    
    methods (Test)
        function testFence(me)
        % Fence plot in 3D
        %   Use this to plot formation
        %
        %   suite = matlab.unittest.TestSuite.fromClass(?org.allnix.matlab.SurfTest, 'ProcedureName', 'testFence');
        fig = me.newFigure();
        Axes = me.newAxes();
        Axes.Parent = fig;
        hold(Axes, 'on');
        
        m = 3; % No. of points in z-direction
        n = 2; % No. of columns
        C = zeros(m,n,3);
        X = zeros(m,n);
        Y = zeros(m,n);
        Z = zeros(m,n);
        
        % size = m x 3
        c = [1,0,0; 0,1,0; 0,0,1];
                
        
        % column 1
        col = 1;
        X(:,col) = [0;0;0];
        Y(:,col) = [0;0;0];
        Z(:,col) = [300;1300;2000];
        C(:,col,:) = c;
        
        col = 2;
        X(:,col) = [500;500;500];
        Y(:,col) = [0;0;0];
        Z(:,col) = [0;1400;2200];
        C(:,col,:) = c;
        
        disp(Z);
        surf(Axes, X,Y,Z, C, 'EdgeColor', 'none');
        
        
        end
    end
end

