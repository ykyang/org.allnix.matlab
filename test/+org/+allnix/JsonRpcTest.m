classdef JsonRpcTest < matlab.unittest.TestCase
    %JSONRPCREQUESTTEST Summary of this class goes here
    %   Detailed explanation goes here
    %   suite = matlab.unittest.TestSuite.fromClass(?org.allnix.JsonRpcTest);
    %   result = run(suite)
    %   disp(result)
    
    properties (Constant)
        logger = org.allnix.getLogger('org.allnix.JsonRpcRequestTest');
        text = '{"jsonrpc": "2.0", "method": "subtract", "params": {"subtrahend": 23, "minuend": 42}, "id": 3}';
    end
    properties (Access = private)
        dataDir = fullfile(pwd, 'data');
    end
    methods (Test)
        function testLoadRequestFromFile(me)
        file = fullfile(me.dataDir, 'jsonrpc_request.json');
        
        req = org.allnix.JsonRpcRequest();
        req.loadFromFile(file);
       
        me.assertEqual(req.getVersion(), '2.0');
        me.assertEqual(req.getMethod(), 'subtract');
        me.assertEqual(req.getId(), 3);
        params = req.getParams();
        me.assertEqual(params.subtrahend, 23);
        me.assertEqual(params.minuend, 42);
        end
        
        function testLoadRequestFromText(me)
        req = org.allnix.JsonRpcRequest();
        
        req.loadFromText(me.text);
        
        me.assertEqual(req.getVersion(), '2.0');
        me.assertEqual(req.getMethod(), 'subtract');
        me.assertEqual(req.getId(), 3);
        params = req.getParams();
        me.assertEqual(params.subtrahend, 23);
        me.assertEqual(params.minuend, 42);
        end
        
        function testRunJsonRpcNoMethodFound(me)
        req = org.allnix.JsonRpcRequest();
        req.loadFromText(me.text);
        
        rep = org.allnix.runJsonRpc(req);
        
        me.assertEqual(rep.getId(), 3);
        me.assertEqual(rep.getErrorCode, -32601);
        end
    end
    
    
    methods
       
    end
end

